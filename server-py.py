from flask import *
app = Flask(__name__)

@app.route('/', methods=['POST'])
def parse_request():
    data = request.json  # data is empty
    # need posted data here
    print("Data body", data)
    return data

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5005, debug=True)